import { useEffect, useState } from 'react';
import './App.scss';
import List from './components/List/List.js';  

function App() {
  const [ productList, setProductList ] = useState([]);

  useEffect(() => {
    fetch ('https://fakestoreapi.com/products/')
    .then((response) => response.json())
    .then((result) => {
      setProductList(result);
    });
  }, []);
 

 

  return (
    <div className="App">
        <List list={productList}></List>
    </div>  
  );
}

export default App;
